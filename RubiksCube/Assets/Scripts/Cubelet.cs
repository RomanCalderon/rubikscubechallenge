﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A pure-data class that represents a Cubelet in a Rubik's cube.
/// </summary>
[System.Serializable]
public class Cubelet
{
    public float x, y, z;
    private Vector3 originalPosition;

    /// <summary>
    /// Constructor that takes in an x, y and z coordinate and sets the originalPosition to that position.
    /// </summary>
    /// <param name="x">The Cubelets x coordinate.</param>
    /// <param name="y">The Cubelets y coordinate.</param>
    /// <param name="z">The Cubelets z coordinate.</param>
    public Cubelet(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;

        originalPosition = new Vector3(x, y, z);
    }

    /// <summary>
    /// Updates the Cubelet's positional data.
    /// </summary>
    /// <param name="x">The new x coordinate.</param>
    /// <param name="y">The new y coordinate.</param>
    /// <param name="z">The new z coordinate.</param>
    public void Update(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /// <summary>
    /// Returns true if the Cubelet is in the center of a face on the cube.
    /// </summary>
    /// <returns></returns>
    public bool IsCenter()
    {
        // Front/back centers
        if ((x == 0f && y == 0f) && z != 0f)
            return true;
        // Right/left centers
        if ((y == 0f && z == 0f) && x != 0f)
            return true;
        // Up/down centers
        if ((z == 0f && x == 0f) && y != 0f)
            return true;
        return false;
    }

    /// <summary>
    /// Gets the positional data of the Cubelet.
    /// </summary>
    /// <returns>The Cubelets position.</returns>
    public Vector3 GetPosition()
    {
        return new Vector3(x, y, z);
    }

    /// <summary>
    /// Returns true if the Cubelet is in its original position when created.
    /// </summary>
    /// <returns></returns>
    public bool InOriginalPosition()
    {
        return GetPosition() == originalPosition;
    }
}
