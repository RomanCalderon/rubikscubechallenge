﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DragDirections
{
    UP,
    DOWN,
    RIGHT,
    LEFT
}

[RequireComponent(typeof(CubeController))]
public class CubeInputController : MonoBehaviour
{
    // The main camera that will cast collision rays
    [SerializeField] new Camera camera;
    [SerializeField] LayerMask cubeletLayerMask;
    [SerializeField] float raycastDistance = 20f;

    // Cube Controller class
    private CubeController cubeController;

    // Mouse drag direction
    private Coroutine dragDirectionCoroutine;
    private Vector2 previousMouePosition = Vector2.zero;
    private DragDirections dragDirection;

    // Cube interaction data
    private Cubelet activeCubelet;
    private Directions rotationDirection;
    private Vector3 cubeletNormal;

    private void Awake()
    {
        // Get a reference to the CubeController class
        cubeController = GetComponent<CubeController>();
    }

    // Update is called once per frame
    void Update()
    {
        #region Mouse Click Input

        /// Mouse click input rotates faces of the cube based on which face has been clicked on
        /// The rotation direction (CW, CCW) is based on which mouse button is pressed (LMB, RMB)

        // Check if the cursor is over a cubelet
        if (CameraController.IsCursorOverCube)
        {
            // Clicking the cube with LMB
            if (Input.GetMouseButtonDown(0))
            {
                // Set the rotation direction to counter-clockwise
                rotationDirection = Directions.CCW;

                // Find the cubelet that the cursor is over
                activeCubelet = GetCurrentCubelet();

                // Send a Cube Interaction object to the Cube Controller
                NewCubeInteraction(new CubeInteraction(activeCubelet, cubeletNormal, rotationDirection));
            }
            // Clicking the cube with RMB
            else if (Input.GetMouseButtonDown(1))
            {
                // Set the rotation direction to clockwise
                rotationDirection = Directions.CW;

                // Find the cubelet that the cursor is over
                activeCubelet = GetCurrentCubelet();

                // Send a Cube Interaction object to the Cube Controller
                NewCubeInteraction(new CubeInteraction(activeCubelet, cubeletNormal, rotationDirection));
            }
        }

        #endregion

        #region Keyboard Input

        /// This block of input was used for testing/debugging the control of the cube
        /// 
        /// F rotates the front face
        /// B rotates the back face
        /// R rotates the right face
        /// L rotates the left face
        /// U rotates the up face
        /// D rotates the down face
        /// X rotates the center x face
        /// Y rotates the center y face
        /// Z rotates the center z face
        /// 
        /// Left Control + any above key press reverses the rotation direction

        // Front
        if (Input.GetKeyDown(KeyCode.F))
            cubeController.AddMove(new Move(Faces.FRONT, (Input.GetKey(KeyCode.LeftControl)) ? Directions.CCW : Directions.CW));
        // Back
        if (Input.GetKeyDown(KeyCode.B))
            cubeController.AddMove(new Move(Faces.BACK, (Input.GetKey(KeyCode.LeftControl)) ? Directions.CCW : Directions.CW));
        // Right
        if (Input.GetKeyDown(KeyCode.R))
            cubeController.AddMove(new Move(Faces.RIGHT, (Input.GetKey(KeyCode.LeftControl)) ? Directions.CCW : Directions.CW));
        // Left
        if (Input.GetKeyDown(KeyCode.L))
            cubeController.AddMove(new Move(Faces.LEFT, (Input.GetKey(KeyCode.LeftControl)) ? Directions.CCW : Directions.CW));
        // Up
        if (Input.GetKeyDown(KeyCode.U))
            cubeController.AddMove(new Move(Faces.UP, (Input.GetKey(KeyCode.LeftControl)) ? Directions.CCW : Directions.CW));
        // Down
        if (Input.GetKeyDown(KeyCode.D))
            cubeController.AddMove(new Move(Faces.DOWN, (Input.GetKey(KeyCode.LeftControl)) ? Directions.CCW : Directions.CW));

        // Center X
        if (Input.GetKeyDown(KeyCode.X))
            cubeController.AddMove(new Move(Faces.CENTER_X, (Input.GetKey(KeyCode.LeftControl)) ? Directions.CCW : Directions.CW));
        // Center Y
        if (Input.GetKeyDown(KeyCode.Y))
            cubeController.AddMove(new Move(Faces.CENTER_Y, (Input.GetKey(KeyCode.LeftControl)) ? Directions.CCW : Directions.CW));
        // Center Z
        if (Input.GetKeyDown(KeyCode.Z))
            cubeController.AddMove(new Move(Faces.CENTER_Z, (Input.GetKey(KeyCode.LeftControl)) ? Directions.CCW : Directions.CW));

        #endregion
    }

    /// <summary>
    /// Sends a CubeInteraction object to the cubeController class.
    /// </summary>
    /// <param name="cubeInteraction">A CubeInteraction object with cube interaction data.</param>
    private void NewCubeInteraction(CubeInteraction cubeInteraction)
    {
        cubeController.ReceiveCubeInteraction(cubeInteraction);
    }

    /// <summary>
    /// Determines which general direction the Vector2 is pointing (UP, DOWN, RIGHT, LEFT).
    /// </summary>
    /// <param name="dir">The raw Vector2.</param>
    private void GetDragDirection(Vector2 dir)
    {
        // A vertical motion
        if (Mathf.Abs(dir.y) > Mathf.Abs(dir.x))
            dragDirection = (Mathf.Sign(dir.y) > 0) ? DragDirections.UP : DragDirections.DOWN;
        // A horizontal motion
        else if (Mathf.Abs(dir.x) > Mathf.Abs(dir.y))
            dragDirection = (Mathf.Sign(dir.x) > 0) ? DragDirections.RIGHT : DragDirections.LEFT;
    }

    /// <summary>
    /// Uses a raycasting technique to find the Cubelet that is under the cursor.
    /// </summary>
    /// <returns>The Cubelet under the cursor.</returns>
    private Cubelet GetCurrentCubelet()
    {
        // If the camera object is null, return null
        if (camera == null)
            return null;

        // Create a ray that is casted from the camera through the cursor
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);

        // If the ray collides with a Cubelet, return that Cubelet object
        if (Physics.Raycast(ray, out RaycastHit hit, raycastDistance, cubeletLayerMask))
        {
            cubeletNormal = hit.normal;
            return hit.transform.GetComponent<CubeletGraphic>().Cubelet;
        }

        // Otherwise return null
        return null;
    }

    /// <summary>
    /// Builds a list of cursor movement deltas and returns (via callback) an average Vector2
    /// </summary>
    /// <param name="resultCallback">The callback containing the average cursor movement delta as a Vector2.</param>
    /// <returns></returns>
    private IEnumerator GetAverageDirection(Action<Vector2> resultCallback)
    {
        List<Vector2> vectors = new List<Vector2>();
        int frames = 0;

        // Build collection of cursor deltas to be averaged
        while (frames < 3)
        {
            vectors.Add(GetMouseDirection());
            frames++;
            yield return null;
        }

        // Remove the first element because it will have an irrelevant, large delta
        vectors.RemoveAt(0);

        // Callback the average of all the cursor deltas
        resultCallback(AverageAllVectors(vectors));

        // Set the coroutine to null so this method can be usable again
        dragDirectionCoroutine = null;
    }

    /// <summary>
    /// Gets the current cursor movement delta.
    /// </summary>
    /// <returns></returns>
    private Vector2 GetMouseDirection()
    {
        Vector2 currentMousePosition = Input.mousePosition;
        float deltaX = (currentMousePosition.x - previousMouePosition.x);
        float deltaY = (currentMousePosition.y - previousMouePosition.y);

        previousMouePosition = currentMousePosition;

        return new Vector3(deltaX, deltaY, 0);
    }

    /// <summary>
    /// Gets the average Vector2 from a list of Vector2s.
    /// </summary>
    /// <param name="list"></param>
    /// <returns></returns>
    private Vector2 AverageAllVectors(List<Vector2> list)
    {
        Vector2 sum = Vector2.zero;

        foreach (Vector2 v in list)
            sum += v;

        return sum / list.Count;
    }
}
