﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Enum for all faces of the cube
public enum Faces
{
    FRONT,
    BACK,
    RIGHT,
    LEFT,
    UP,
    DOWN,
    CENTER_X,
    CENTER_Y,
    CENTER_Z
}

// Enum for directions a face can rotate
public enum Directions
{
    CW, // Clockwise
    CCW // Counter-clockwise, or "prime"
}

public class CubeController : MonoBehaviour
{
    // Event handler for when the cube has been created
    // Passes the 3D array of cubelets that make up the cube
    public delegate void CubeCreationHandler(Cubelet[,,] cube);
    public static event CubeCreationHandler OnCubeCreated;

    // Event handler for when a face on the cube has changed
    // Passes a list of the Cubelets in the face that changes
    // Passes the face as an enum
    // Passes the rotation direction as an enum
    public delegate void CubeMoveHandler(List<Cubelet> cubes, Faces face, Directions direction);
    public static event CubeMoveHandler OnFaceChanged;

    // Event handler for when the cube has been solved
    public delegate void CubeStateHandler();
    public static event CubeStateHandler OnCubeSolved;

    // Flag for when a move is in progress
    private bool processingMove = false;

    // The 3D array representation of the Rubik's cube
    // Composed of Cubelets
    private Cubelet[,,] cube = new Cubelet[3,3,3];

    // Flag for testing, toggles whether a scramble should happen at the start
    [SerializeField] bool scrambleCube = true;
    // Number of moves for the scramble
    [SerializeField] int scrambleMoves = 16;

    // Queue of move actions
    // This allows for multiple rapid move commands and then executing each one in order
    private Queue<Action> moves = new Queue<Action>();

    private void Awake()
    {
        CubeGraphicsController.OnFinishedMove += CubeGraphicsController_OnFinishedMove;
    }

    private void OnDestroy()
    {
        // Remove all subscribers from these events
        // when this object is destroyed (i.e. Scene reloads)
        OnCubeCreated = null;
        OnFaceChanged = null;
        OnCubeSolved = null;
    }

    // Start is called before the first frame update
    void Start()
    {
        // Create the cube
        GenerateCube();

        // Scramble the cube
        if (scrambleCube)
            StartCoroutine(ScrambleCube());
    }

    private IEnumerator ScrambleCube()
    {
        // Wait a second, then start the scramble
        yield return new WaitForSeconds(1f);

        // Add a few random moves to the moves queue
        for (int i = 0; i < scrambleMoves; i++)
            AddMove(new Move((Faces)UnityEngine.Random.Range(0, 6), (Directions)UnityEngine.Random.Range(0, 2)));
    }

    private void Update()
    {
        // If the moves queue is not empty,
        // execute the next move in the queue
        if (moves.Count > 0)
            NextMove();
    }

    /// <summary>
    /// Populates the three-dimensional array with Cubelets, assigning them thier respective positions.
    /// Calls the OnCubeCreated event when finished.
    /// </summary>
    private void GenerateCube()
    {
        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                for (int z = 0; z < 3; z++)
                {
                    cube[x, y, z] = new Cubelet(x - 1, y - 1, z - 1);
                }
            }
        }

        OnCubeCreated?.Invoke(cube);
    }

    /// <summary>
    /// Gets the current cubelet array.
    /// </summary>
    /// <returns>The cubelet array.</returns>
    public Cubelet[,,] GetCube()
    {
        return cube;
    }

    /// <summary>
    /// Takes in a CubeInteraction object storing the interaction data of a Cubelet.
    /// Adds a move command to the queue for processing
    /// </summary>
    /// <param name="cubeInteraction"></param>
    public void ReceiveCubeInteraction(CubeInteraction cubeInteraction)
    {
        Faces rotationFace = Faces.FRONT;
        
        // Right/Left faces
        if (Mathf.Abs(cubeInteraction.InteractionNormal.x) > Mathf.Abs(cubeInteraction.InteractionNormal.y) &&
            Mathf.Abs(cubeInteraction.InteractionNormal.x) > Mathf.Abs(cubeInteraction.InteractionNormal.z))
            rotationFace = (Mathf.Sign(cubeInteraction.InteractionNormal.x) > 0) ? Faces.RIGHT : Faces.LEFT;
        // Front/Back faces
        else if (Mathf.Abs(cubeInteraction.InteractionNormal.z) > Mathf.Abs(cubeInteraction.InteractionNormal.x) &&
            Mathf.Abs(cubeInteraction.InteractionNormal.z) > Mathf.Abs(cubeInteraction.InteractionNormal.y))
            rotationFace = (Mathf.Sign(cubeInteraction.InteractionNormal.z) > 0) ? Faces.BACK : Faces.FRONT;
        // Up/Down faces
        else if (Mathf.Abs(cubeInteraction.InteractionNormal.y) > Mathf.Abs(cubeInteraction.InteractionNormal.x) &&
            Mathf.Abs(cubeInteraction.InteractionNormal.y) > Mathf.Abs(cubeInteraction.InteractionNormal.z))
            rotationFace = (Mathf.Sign(cubeInteraction.InteractionNormal.y) > 0) ? Faces.UP : Faces.DOWN;

        print(rotationFace.ToString() + " face");

        AddMove(new Move(rotationFace, cubeInteraction.Direction));
    }

    /// <summary>
    /// Locates all Cubelets on the specified face of the cube and adds them to a list called layer.
    /// Invokes the OnFaceChanged event and passes the layer, face and direction data to the event handler.
    /// </summary>
    /// <param name="face">The face to be rotated.</param>
    /// <param name="dir">The direction of rotation.</param>
    private void TurnFace(Faces face, Directions dir)
    {
        // Don't execute a face rotation if a one is being processed already
        if (processingMove)
            return;

        processingMove = true;

        // The list of Cubelets in the face that needs to be rotated
        List<Cubelet> layer = new List<Cubelet>();

        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                for (int z = 0; z < 3; z++)
                {
                    switch (face)
                    {
                        case Faces.FRONT:
                            if (cube[x, y, z].z == -1)
                                layer.Add(cube[x, y, z]);
                            break;
                        case Faces.BACK:
                            if (cube[x, y, z].z == 1)
                                layer.Add(cube[x, y, z]);
                            break;
                        case Faces.RIGHT:
                            if (cube[x, y, z].x == 1)
                                layer.Add(cube[x, y, z]);
                            break;
                        case Faces.LEFT:
                            if (cube[x, y, z].x == -1)
                                layer.Add(cube[x, y, z]);
                            break;
                        case Faces.UP:
                            if (cube[x, y, z].y == 1)
                                layer.Add(cube[x, y, z]);
                            break;
                        case Faces.DOWN:
                            if (cube[x, y, z].y == -1)
                                layer.Add(cube[x, y, z]);
                            break;
                        case Faces.CENTER_X:
                            if (cube[x, y, z].x == 0)
                                layer.Add(cube[x, y, z]);
                            break;
                        case Faces.CENTER_Y:
                            if (cube[x, y, z].y == 0)
                                layer.Add(cube[x, y, z]);
                            break;
                        case Faces.CENTER_Z:
                            if (cube[x, y, z].z == 0)
                                layer.Add(cube[x, y, z]);
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        OnFaceChanged?.Invoke(layer, face, dir);
    }

    /// <summary>
    /// Adds a move command to the queue.
    /// </summary>
    /// <param name="move">The move command.</param>
    public void AddMove(Move move)
    {
        // move cannot be null
        if (move == null)
            return;

        // Add the new move to the queue
        moves.Enqueue(delegate { TurnFace(move.Face, move.Direction); });
    }

    /// <summary>
    /// Dequeues the next move for the queue and invokes its action
    /// </summary>
    private void NextMove()
    {
        if (processingMove)
            return;

        moves.Dequeue().Invoke();
    }

    /// <summary>
    /// Checks if the cube is in the solved state.
    /// </summary>
    private bool IsSolved()
    {
        // Loop through all of the Cubelets and check if thier position
        // matches thier original position (solved state).
        for (int x = 0; x < 3; x++)
            for (int y = 0; y < 3; y++)
                for (int z = 0; z < 3; z++)
                    if (!cube[x, y, z].InOriginalPosition())
                        return false;

        // If the loop finishes, that means every Cubelet is in thier solved position
        // Return true!
        return true;
    }

    #region Event Listeners

    /// <summary>
    /// Event listener for when a move has finished processing.
    /// </summary>
    private void CubeGraphicsController_OnFinishedMove()
    {
        processingMove = false;

        // Check if the cube has been solved and there are no more moves in the queue
        // This additional check will help prevent the cube from saying it's solved when there are more scramble moves
        // being applied. More like a safety net for getting a random permutation that leads to a premature solve
        if (IsSolved() && moves.Count == 0)
            OnCubeSolved?.Invoke();
    }

    #endregion
}
