﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Data class used for storing an interaction with the cube.
/// Cubelet             - The Cubelet that the user has dragged on
/// InteractionNormal   - The side of the Cubelet that was dragged on
/// DragDirection       - The direction the user wants to rotate the face
/// </summary>
public class CubeInteraction
{
    public Cubelet Cubelet;
    public Vector3 InteractionNormal;
    public Directions Direction;

    public CubeInteraction(Cubelet cubelet, Vector3 interactionNormal, Directions direction)
    {
        Cubelet = cubelet;
        InteractionNormal = interactionNormal;
        Direction = direction;
    }

    public override string ToString()
    {
        return "[Cubelet] " + Cubelet.GetPosition() + " | [Normal] " + InteractionNormal + " | [Drag Direction] " + Direction.ToString();
    }
}
