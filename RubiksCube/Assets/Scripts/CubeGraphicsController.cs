﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class CubeGraphicsController : MonoBehaviour
{
    // Event handler for when a move on the cube has finished
    public delegate void MoveHandler();
    public static event MoveHandler OnFinishedMove;
    
    // A sound effect for when a face on the cube is rotated
    private AudioSource audioSource;
    [SerializeField] AudioClip cubeTurnSound;

    // A reference to the Cubelet prefab
    [SerializeField] GameObject cubeletPrefab;

    // The transform that holds all of the Cubelet GameObjects
    [SerializeField] Transform cubeHolder;
    
    // List of all cubelets that make up the rubik's cube
    [SerializeField] List<CubeletGraphic> cubelets = new List<CubeletGraphic>();
    private Cubelet[,,] cubeletsArray;

    // The speed at which the faces rotate
    [SerializeField] float rotationSpeed = 12f;

    // List of cubelets in a layer/face
    List<CubeletGraphic> cubeletsLayer = new List<CubeletGraphic>();

    // Temporary object used for storing a face of cubelets
    GameObject rotationObject;
    // Temorary object used for tracking rotationObject's progress
    GameObject targetObject;

    // Coroutine for tracking IEnumerator state
    Coroutine rotationCoroutine;

    // Used for the Easter egg :P
    private bool updatingCubeletMaterials = false;
    [SerializeField] Material centerMaterial;


    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();

        // Subscribe to all the CubeController events
        CubeController.OnCubeCreated += CubeController_OnCubeCreated;
        CubeController.OnFaceChanged += CubeController_OnFaceChanged;
        CubeController.OnCubeSolved += CubeController_OnCubeSolved;
    }

    private void OnDestroy()
    {
        // Remove all subscribers from the event
        // when this object is destroyed (i.e. Scene reloads)
        OnFinishedMove = null;
    }

    /// <summary>
    /// Physically rotate a face of the cube.
    /// </summary>
    /// <param name="callback">Callback for when the rotation has finished.</param>
    /// <returns></returns>
    private IEnumerator BeginRotation(Action callback)
    {
        // The difference in angles between the faces current rotation and its target rotation
        float angleDiff = Quaternion.Angle(rotationObject.transform.rotation, targetObject.transform.rotation);

        // Play the rubik's cube turn sfx
        audioSource.PlayOneShot(cubeTurnSound);

        // Rotate until the difference in angles is less than or equal to 2 degress
        while (angleDiff > 2)
        {
            // Interpolate the rotationObject's rotation to the targetObject's rotation
            rotationObject.transform.rotation = Quaternion.Lerp(rotationObject.transform.rotation, targetObject.transform.rotation, Time.deltaTime * rotationSpeed);

            // Update angleDiff calculation
            angleDiff = Quaternion.Angle(rotationObject.transform.rotation, targetObject.transform.rotation);

            yield return null;
        }

        // The rotation will be a little incomplete so just snap the rotationObject to the targetObject
        rotationObject.transform.rotation = targetObject.transform.rotation;

        // Invoke the callback since the rotation job has finished
        callback();
    }

    /// <summary>
    /// Cleans up the rotation job by moving all the rotated Cubelets back into the holder object,
    /// updating all of thier positional data, and eventually destroying the rotationObject and targetObject.
    /// Invokes the OnFinishedMove event.
    /// </summary>
    private void FinishedRotation()
    {
        foreach (CubeletGraphic c in cubeletsLayer)
        {
            // Place the cubelets graphic back into the main holder (rubik's cube)
            c.transform.SetParent(cubeHolder.transform);

            // Update the Cubelet's positional data
            c.Cubelet.Update(Mathf.RoundToInt(c.transform.position.x / 2), Mathf.RoundToInt(c.transform.position.y / 2), Mathf.RoundToInt(c.transform.position.z / 2));
        }

        // Destroy the rotation and target objects
        Destroy(rotationObject);
        Destroy(targetObject);

        // Invoke the OnFinishedMove event
        OnFinishedMove?.Invoke();
    }

    #region Event Listeners

    /// <summary>
    /// Event listener for when the CubeController made a change to one of its faces.
    /// Updates the corresponding face by rotating it.
    /// </summary>
    /// <param name="cubes"></param>
    /// <param name="face"></param>
    /// <param name="direction"></param>
    private void CubeController_OnFaceChanged(List<Cubelet> cubes, Faces face, Directions direction)
    {
        // Clear the list, we don't want the old data
        cubeletsLayer.Clear();

        // Populate the layer list with cubelets on the correct face of the cube
        foreach (CubeletGraphic cg in cubelets)
            foreach (Cubelet c in cubes)
                if (cg.Cubelet == c)
                    cubeletsLayer.Add(cg);

        // Create a temporary holder for the layer of cubelets that need to be rotated
        rotationObject = new GameObject();
        rotationObject.transform.position = Vector3.zero;
        cubeletsLayer.ForEach(c => c.transform.SetParent(rotationObject.transform));

        // Used for tracking rotation progress
        targetObject = new GameObject();
        targetObject.transform.position = Vector3.zero;

        // Rotate the rotationObject on the respective axis and direction
        switch (face)
        {
            case Faces.FRONT:
                targetObject.transform.Rotate(Vector3.forward, (direction == Directions.CW) ? -90 : 90);
                break;
            case Faces.BACK:
                targetObject.transform.Rotate(Vector3.forward, (direction == Directions.CW) ? 90 : -90);
                break;
            case Faces.RIGHT:
                targetObject.transform.Rotate(Vector3.right, (direction == Directions.CW) ? 90 : -90);
                break;
            case Faces.LEFT:
                targetObject.transform.Rotate(Vector3.right, (direction == Directions.CW) ? -90 : 90);
                break;
            case Faces.UP:
                targetObject.transform.Rotate(Vector3.up, (direction == Directions.CW) ? 90 : -90);
                break;
            case Faces.DOWN:
                targetObject.transform.Rotate(Vector3.up, (direction == Directions.CW) ? -90 : 90);
                break;
            case Faces.CENTER_X:
                targetObject.transform.Rotate(Vector3.right, (direction == Directions.CW) ? -90 : 90);
                break;
            case Faces.CENTER_Y:
                targetObject.transform.Rotate(Vector3.up, (direction == Directions.CW) ? -90 : 90);
                break;
            case Faces.CENTER_Z:
                targetObject.transform.Rotate(Vector3.forward, (direction == Directions.CW) ? -90 : 90);
                break;
            default:
                break;
        }

        // Start the rotation "animation"
        rotationCoroutine = StartCoroutine(BeginRotation(FinishedRotation));
    }

    /// <summary>
    /// When the cube has been created in the Cube Controller class, create a visual representation of the cube with a 3D structure of Cubelet GameObjects.
    /// </summary>
    /// <param name="cube">The 3D array of Cubelets</param>
    private void CubeController_OnCubeCreated(Cubelet[,,] cube)
    {
        cubeletsArray = cube;

        // Used for assigning each Cubelet GameObject a value to thier name
        int index = 0;

        for (int x = 0; x < 3; x++)
        {
            for (int y = 0; y < 3; y++)
            {
                for (int z = 0; z < 3; z++)
                {
                    // Instantiate a cubelet graphic in the correct position
                    Vector3 cubletPosition = new Vector3(x - 1, y - 1, z - 1);
                    CubeletGraphic cubeletGraphic = Instantiate(cubeletPrefab, cubletPosition * 2, Quaternion.identity, cubeHolder).GetComponent<CubeletGraphic>();
                    
                    // Give it a reference to the cubelet data object
                    cubeletGraphic.Initialize(cube[x, y, z]);
                    
                    // Rename the cubelet in the scene
                    cubeletGraphic.gameObject.name = "Cubelet " + index;
                    index++;
                    
                    // Set the material to the white material (part of the easter egg)
                    if (cubeletGraphic.Cubelet.IsCenter())
                        cubeletGraphic.MeshRenderer.material = centerMaterial;

                    // Add this cubelet graphic to the list
                    cubelets.Add(cubeletGraphic);
                }
            }
        }
    }
    
    /// <summary>
    /// Event listener for when the cube has been solved.
    /// Starts the texture fade effect.
    /// </summary>
    private void CubeController_OnCubeSolved()
    {
        StartCoroutine(FadeTexture());
    }

    #endregion

    // A little Easter egg :)
    private IEnumerator FadeTexture()
    {
        // If this flag is true, don't allow this Enumerator to execute
        if (updatingCubeletMaterials)
            yield break;

        updatingCubeletMaterials = true;

        int iterations = 30;

        float textureBlend = 0;

        yield return new WaitForSeconds(0.5f);

        while (iterations > 0)
        {
            foreach (CubeletGraphic cg in cubelets)
            {
                // Blend the texture to the orange texture
                textureBlend = Mathf.Lerp(textureBlend, 1, Time.deltaTime / 2f);

                cg.MeshRenderer.material.SetFloat("_Blend", textureBlend);
            }

            yield return null;

            iterations--;
        }

        updatingCubeletMaterials = false;
    }
}
