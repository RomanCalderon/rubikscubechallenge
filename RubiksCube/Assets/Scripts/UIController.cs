﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/// <summary>
/// Controlls all UI-related elements.
/// Allows the user to set a timer, start/stop the timer, access directions, open an About menu, reset the scene and exit the application.
/// </summary>
public class UIController : MonoBehaviour
{
    // Popup UI elements
    [SerializeField] GameObject setTimerConfigUI;
    [SerializeField] GameObject directionsUI;
    [SerializeField] GameObject aboutUI;

    // Input elements for setting the timer
    [SerializeField] InputField minutesInput;
    [SerializeField] InputField secondsInput;
    
    // Keeps track of which popup is open
    private int activePopup = -1;
    
    // UI elements for the timer interactions
    [SerializeField] Text startStopTimerText;
    [SerializeField] Button startStopTimerButton;
    [SerializeField] Button setTimerButton;

    // The timer componenet
    [SerializeField] Timer timer;
    // The text UI that displays the timer's current time
    [SerializeField] Text timerText;


    private void Awake()
    {
        // Event subscriptions
        timer.OnTimerStarted += Timer_OnTimerStarted;
        timer.OnTimerStopped += Timer_OnTimerStopped;
        timer.OnTimerFinished += Timer_OnTimerFinished;
        CubeController.OnCubeSolved += CubeController_OnCubeSolved;

        // The start/stop buttons' behaviour changes based on if the timer is counting down or not
        // At first, the timer is not counting down, so add the StartTimerButton listener for when the button is pressed
        startStopTimerButton.onClick.RemoveAllListeners();
        startStopTimerButton.onClick.AddListener(delegate { StartTimerButton(); });
    }

    private void Start()
    {
        // Set the timer to 1 minute by default
        timer.SetTimer(1, 0);

        // Update the timer text
        timerText.text = timer.GetTime();
    }

    private void Update()
    {
        // Update the timerText every frame if the timer is counting down
        if (timer.IsCountingDown)
            timerText.text = timer.GetTime();
    }

    #region Timer Control Functions

    private void StartTimerButton()
    {
        print("Start the timer!");

        // Start the timer
        timer.StartTimer();
        
        // Set the Set Timer button to inactive
        setTimerButton.interactable = false;
    }

    private void StopTimerButton()
    {
        print("Stop the timer!");

        // Stop the timer
        timer.StopTimer(true);

        // Update the timer text
        timerText.text = timer.GetTime();

        // Set the Set Timer button to active
        setTimerButton.interactable = true;
    }
    
    public void SetTimerButton()
    {
        TogglePopup(0);
    }
    
    public void SetButton()
    {
        // Set the adjusted minutes and seconds
        timer.SetTimer(int.Parse(minutesInput.text), int.Parse(secondsInput.text));

        // Update the text ui
        timerText.text = timer.GetTime();

        // Close the config timer popup
        ClosePopups();
    }

    #endregion

    #region Directions/About/Reset/Exit Buttons

    public void DirectionButton()
    {
        TogglePopup(1);
    }

    public void AboutButton()
    {
        TogglePopup(2);
    }

    public void ResetButton()
    {
        // Reload the scene
        SceneManager.LoadScene(0);
    }

    public void ExitButton()
    {
        Application.Quit();
    }

    #endregion

    /// <summary>
    /// Opens a popup based on index.
    /// 0 - set timer config popup
    /// 1 - directions popup
    /// 2 - about popup
    /// </summary>
    /// <param name="index"></param>
    public void TogglePopup(int index)
    {
        ClosePopups();

        switch (index)
        {
            case 0:
                setTimerConfigUI.SetActive(index != activePopup);
                break;
            case 1:
                directionsUI.SetActive(index != activePopup);
                break;
            case 2:
                aboutUI.SetActive(index != activePopup);
                break;
            default:
                break;
        }

        activePopup = (!PopupActive()) ? -1 : index;
    }

    /// <summary>
    /// Closes all popup UI elements
    /// </summary>
    public void ClosePopups()
    {
        setTimerConfigUI.SetActive(false);
        directionsUI.SetActive(false);
        aboutUI.SetActive(false);
    }

    /// <summary>
    /// Returns true if a popup is active.
    /// </summary>
    /// <returns></returns>
    private bool PopupActive()
    {
        return setTimerConfigUI.activeSelf || directionsUI.activeSelf || aboutUI.activeSelf;
    }

    #region Event Listeners

    /// <summary>
    /// Event listener for when the timer has started.
    /// </summary>
    private void Timer_OnTimerStarted()
    {
        // Set the "START TIMER" text to "STOP TIMER"
        startStopTimerText.text = "STOP TIMER";

        // When the timer has started, add the StopTimerButton listener for when the button is pressed
        startStopTimerButton.onClick.RemoveAllListeners();
        startStopTimerButton.onClick.AddListener(delegate { StopTimerButton(); });
    }

    /// <summary>
    /// Event listener for when the timer has stopped.
    /// </summary>
    private void Timer_OnTimerStopped()
    {
        // Set the "STOP TIMER" text to "START TIMER"
        startStopTimerText.text = "START TIMER";

        // When the timer is stopped, add the StartTimerButton listener for when the button is pressed
        startStopTimerButton.onClick.RemoveAllListeners();
        startStopTimerButton.onClick.AddListener(delegate { StartTimerButton(); });

        // Update the timer text
        timerText.text = timer.GetTime();

        // Set the Set Timer button to active
        setTimerButton.interactable = true;
    }

    /// <summary>
    /// Event listener for when the timer has finished.
    /// </summary>
    private void Timer_OnTimerFinished()
    {
        // Set the "STOP TIMER" text to "START TIMER"
        startStopTimerText.text = "START TIMER";

        startStopTimerButton.onClick.RemoveAllListeners();
        startStopTimerButton.onClick.AddListener(delegate { StartTimerButton(); });

        // Update the timer text
        timerText.text = timer.GetTime();

        // Set the Set Timer button to active
        setTimerButton.interactable = true;
    }
    
    /// <summary>
    /// Event listener for when the cube has been solved. The event stops the timer.
    /// </summary>
    private void CubeController_OnCubeSolved()
    {
        // Stop the timer
        timer.StopTimer(false);
    }

    #endregion
}
