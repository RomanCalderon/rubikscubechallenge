﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A component used on the Cubelet GameObject.
/// Stores a reference to a Cubelet object.
/// </summary>
public class CubeletGraphic : MonoBehaviour
{
    public Cubelet Cubelet;

    // Added for ease of access
    // Part of the Easter egg
    public MeshRenderer MeshRenderer;
    
    /// <summary>
    /// Sets its reference to a Cubelet.
    /// </summary>
    /// <param name="c">Cubelet reference.</param>
    public void Initialize(Cubelet c)
    {
        Cubelet = c;
    }
}
