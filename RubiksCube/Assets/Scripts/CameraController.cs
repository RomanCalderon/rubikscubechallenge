﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    // Can the camera be panned/repositioned?
    public static bool CanControl = true;
    // Flag for when the camera is being panned/dragging
    public static bool IsPanningCamera { get; private set; }
    // Flag for when the cursor is over the cube
    public static bool IsCursorOverCube { get; private set; }

    [SerializeField] Transform target;
    [SerializeField] new Camera camera;
    [SerializeField] float distance = 2f;
    [SerializeField] Vector2 rotationSpeed = new Vector2(10f, 10f);
    [SerializeField] float minVerticalLimit = -90f, maxVerticalLimit = 90f;
    [SerializeField] float smoothTime = 3f;

    // Flag for when there is a valid rotation
    // This is when the cursor is over the background, not the cube
    private bool validRotation = true;

    // The rotation that will be applied to this transform
    private Vector2 rotation = Vector2.zero;
    // The speed of the rotation
    private Vector2 velocity = Vector2.zero;
    // A flag for when the camera is upside down and the controls need to be inverted
    // This helps with making the rotation/panning of the camera still feel natural
    private bool viewInverted = false;


    // Start is called before the first frame update
    void Start()
    {
        // Get the transforms initial rotation (offset)
        rotation = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, 0f);
    }

    private void FixedUpdate()
    {
        // Checks if a camera pan is valid based on cursor position
        // validRotation is only true if the cursor is NOT over a collider (the cube)
        if (camera != null && !IsPanningCamera)
        {
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            IsCursorOverCube = Physics.Raycast(ray, distance * 4);
            validRotation = !IsCursorOverCube;
        }
    }
    
    void LateUpdate()
    {
        // Only rotate if a target is specified
        if (target != null)
            Rotate();
    }

    /// <summary>
    /// Manages the user input and rotation of this transform.
    /// </summary>
    private void Rotate()
    {
        // Checks if the user is panning the camera
        if (Input.GetMouseButtonDown(0))
            IsPanningCamera = true;
        else if (Input.GetMouseButtonUp(0))
            IsPanningCamera = false;

        // Get input if the left mouse button is down, the canRotate flag is true AND the validRotation flag is true
        if (Input.GetMouseButton(0) && CanControl && validRotation)
            velocity = new Vector2(Input.GetAxis("Mouse X") * rotationSpeed.x, Input.GetAxis("Mouse Y") * rotationSpeed.y) * 0.05f;

        // Check if the camera is upside down
        viewInverted = Mathf.Abs(rotation.x) > 90 && Mathf.Abs(rotation.x) < 270;

        // Apply rotational velocity to the rotation
        // If the view is inverted, invert the horizontal input
        rotation.y += (viewInverted) ? -velocity.x : velocity.x;
        rotation.x -= velocity.y;
        rotation.x = ClampAngle(rotation.x, minVerticalLimit, maxVerticalLimit);
        
        // Get the new rotation as a Quaternion
        Quaternion newRotation = Quaternion.Euler(rotation.x, rotation.y, 0);

        // Apply the rotation
        transform.rotation = newRotation;

        // Decrease velocity over time (drag)
        velocity = Vector2.Lerp(velocity, Vector2.zero, Time.deltaTime * smoothTime);
    }

    /// <summary>
    /// Helper function for clamping an angle between a minimum value and maximum value.
    /// </summary>
    /// <param name="angle">The angle to clamp.</param>
    /// <param name="min">The minimum value.</param>
    /// <param name="max">The maximum value.</param>
    /// <returns></returns>
    private float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360f)
            angle += 360f;

        if (angle > 360f)
            angle -= 360f;

        return Mathf.Clamp(angle, min, max);
    }
}
