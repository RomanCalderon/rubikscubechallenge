﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A simple countdown timer class.
/// This timer keeps track of minutes and seconds.
/// </summary>
public class Timer : MonoBehaviour
{
    // Event handler for when the timer has started, stopped and finished.
    public delegate void TimeProgressHandler();
    public event TimeProgressHandler OnTimerStarted;
    public event TimeProgressHandler OnTimerStopped;
    public event TimeProgressHandler OnTimerFinished;

    // Number of minutes remaining
    private int minutes;
    // Number of seconds remaining
    private int seconds;

    // When the timer is set, that amount of time is stored into originalTime
    private float originalTime;
    // The amount of time remaining in seconds
    private float timeRemaining;
    // Flag for when the timer is currently counting down
    public bool IsCountingDown { get; private set; } = false;
    
    /// <summary>
    /// Sets the timer.
    /// </summary>
    /// <param name="m">Number of minutes.</param>
    /// <param name="s">Number of seconds.</param>
    public void SetTimer(int m, int s)
    {
        // Clamp the passed values from 0 - 59
        minutes = Mathf.Clamp(m, 0, 59);
        seconds = Mathf.Clamp(s, 0, 59);

        // Set both the originalTime and remainingTime to the equivalent number of seconds
        originalTime = timeRemaining = (minutes * 60f + seconds);
    }

    /// <summary>
    /// Starts the timer.
    /// </summary>
    public void StartTimer()
    {
        // Set the countdown flag to true
        IsCountingDown = true;

        // Invoke the OnTimerStarted event
        OnTimerStarted?.Invoke();
    }

    private void Update()
    {
        // Countdown the time
        if (timeRemaining > 0f && IsCountingDown)
        {
            // Update countdown values
            timeRemaining -= Time.deltaTime;
            minutes = GetMinutes();
            seconds = GetSeconds();

            // When the timer reaches zero,
            // call Finsihed()
            if (timeRemaining <= 0)
            {
                timeRemaining = 0;
                Finished();
            }
        }
    }

    /// <summary>
    /// Stops the timer.
    /// </summary>
    /// <param name="resetTimer">If true, the timer will reset its values back to the original set values.</param>
    public void StopTimer(bool resetTimer)
    {
        // Set the countdown flag to false
        IsCountingDown = false;

        // Set the timeRemaining back to its original set value
        if (resetTimer)
            timeRemaining = originalTime;

        // Invoke the OnTimerStopped event
        OnTimerStopped?.Invoke();
    }

    /// <summary>
    /// Called when the timer has finished.
    /// </summary>
    private void Finished()
    {
        IsCountingDown = false;

        // Invoke the OnTimerFinished event
        OnTimerFinished?.Invoke();
    }

    /// <summary>
    /// Gets the current time remaining as a formatted string. (00:00)
    /// </summary>
    /// <returns>A formatted string that represents the time remaining.</returns>
    public string GetTime()
    {
        return GetMinutes() + ":" + GetSeconds().ToString("00");
    }

    /// <summary>
    /// Gets the current amount of minutes remaining.
    /// </summary>
    /// <returns></returns>
    public int GetMinutes()
    {
        return Mathf.FloorToInt(timeRemaining / 60f);
    }

    /// <summary>
    /// Gets the current amount of seconds remaining.
    /// </summary>
    /// <returns></returns>
    public int GetSeconds()
    {
        return Mathf.FloorToInt(timeRemaining % 60f);
    }

    private void OnDestroy()
    {
        // Remove all subscribers from these events
        // when this object is destroyed (i.e. Scene reloads)
        OnTimerStarted = null;
        OnTimerStopped = null;
        OnTimerFinished = null;
    }

}
