﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A simple, pure-data class responsible for storing a Face and Direction that will be applied as a Move to the cube.
/// </summary>
public class Move
{
    // The face of the cube that will be manipulated
    public Faces Face { get; private set; }
    // The direction the face will rotate
    public Directions Direction { get; private set; }

    // Constructor for initializing the object and its data
    public Move(Faces face, Directions direction)
    {
        Face = face;
        Direction = direction;
    }
}
